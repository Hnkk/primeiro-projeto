import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor() { }

  public getUsuario(): Usuario{
    let usuario = new Usuario()
    usuario.nome = "Hinokuma"
    usuario.email = "Hinokuma@pontocom.com"
    return usuario
  }
  public listaUsuario():Usuario[]{
    return [{
      nome:"iohan",
      email:"iohan15@hotmail.com"
      },
      {
        nome:"jose",
        email:"email@do@jose.com"
      },
      {
        nome:"maria do rosario",
        email:"reclame aqui hoje@@@"
      }
    
    ]
  }

   

  
}
